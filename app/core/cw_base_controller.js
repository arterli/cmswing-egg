const { Controller } = require('egg');
class BaseController extends Controller {
  get user() {
    return this.ctx.session.user;
  }

  /**
   *  统一返回
   * @param {any} data 返回数据
   */
  success(data) {
    this.ctx.body = {
      success: true,
      errorCode: '0',
      data,
      errorMessage: 'ok',
    };

  }

  /**
   * 统一错误返回
   * @param {string} errorMessage 错误信息
   * @param {string} errorCode 错误代码
   * @param {any} data 返回数据
   */
  fail(errorMessage, errorCode = '1000', data) {
    this.ctx.body = {
      success: true,
      errorCode,
      data,
      errorMessage,
    };
  }
  notFound(msg) {
    msg = msg || 'not found';
    this.ctx.throw(404, msg);
  }
}
module.exports = BaseController;
