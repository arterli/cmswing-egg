'use strict';
module.exports = app => {
  // 后台生成 web路由
  require('./router/webRouter')(app);
  // 自定义路由
  // const { router, controller } = app;
  // router.get('/', controller.home.index);
};
