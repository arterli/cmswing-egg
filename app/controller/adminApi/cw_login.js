/* eslint-disable jsdoc/check-tag-names */
'use strict';
const Controller = require('./base');
/**
* @controller cw_login 后台登录
*/
class CwLoginController extends Controller {
  /**
   * @summary 用户登录
   * @description 用户登录接口
   * @router post /adminApi/cw_login/account
   * @request body accountRequest *body 请求信息
   * @response 200 accountResponse 用户登录信息
   */
  async account() {
    const { ctx } = this;
    let { username, password, type } = ctx.request.body;
    password = ctx.helper.cw.cipher(password);
    const user = await ctx.model.CwUser.findOne({
      where: {
        username,
        password,
      },
    });
    if (user) {
      const token = ctx.helper.cw.generateToken(user);
      ctx.body = {
        success: true,
        token,
        msg: '登录成功',
        type,
      };
    } else {
      ctx.body = {
        success: false,
        msg: '账户或密码错误',
        type,
      };
    }
  }
}
module.exports = CwLoginController;
