'use strict';

const Controller = require('./base');
class CwIndexController extends Controller {
  async index() {
    const { ctx } = this;
    await ctx.render('admin/index');
  }
}

module.exports = CwIndexController;
