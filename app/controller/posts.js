'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    this.ctx.body = ctx.url;
  }
  async new() {
    const { ctx } = this;
    this.ctx.body = ctx.url;
  }
  async show() {
    const { ctx } = this;
    ctx.body = ctx.params;
  }
}

module.exports = HomeController;
