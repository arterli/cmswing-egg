'use strict';

const Controller = require('egg').Controller;
class HomeController extends Controller {
  async index() {
    // this.ctx.body = 'hi, ' + this.app.plugins.swagger.name;
    const { ctx } = this;
    // const data1 = ctx.helper.cw.cipher('123456');
    // console.log(data1);
    // const data2 = ctx.helper.cw.decipher(data1);
    // console.log(data2);
    // const dd = await ctx.service.cwGenerateRoute.run();
    // const mm = await ctx.service.cwMiddleware.getList();
    // await ctx.service.cwController.getFun();
    const rbac = await ctx.service.cwRbac.check('/sys/user', 11);
    console.log(rbac);
    await ctx.render('index');
  }
}

module.exports = HomeController;
