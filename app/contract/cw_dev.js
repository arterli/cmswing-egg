'use strict';
module.exports = {
  menuItem: {
    id: {
      type: 'integer',
      example: 'id',
    },
    name: {
      type: 'string',
      example: '路由名称',
    },
    path: {
      type: 'string',
      example: '路由 URL 路径',
    },
    icon: {
      type: 'string',
      example: '配置菜单的图标，默认使用 antd 的 icon 名，默认不适用二级菜单的 icon',
    },
    access: {
      type: 'string',
      example: '权限配置，需要预先配置权限',
    },
    hideChildrenInMenu: {
      type: 'boolean',

      example: '用于隐藏不需要在菜单中展示的子路由',
    },
    hideInMenu: {
      type: 'boolean',

      example: '可以在菜单中不展示这个路由，包括子路由',
    },
    hideInBreadcrumb: {
      type: 'boolean',

      example: '可以在面包屑中不展示这个路由，包括子路由。',
    },
    headerRender: {
      type: 'boolean',

      example: '当前路由不展示顶栏',
    },
    footerRender: {
      type: 'boolean',

      example: '当前路由不展示页脚',
    },
    menuRender: {
      type: 'boolean',

      example: '当前路由不展示菜单',
    },
    menuHeaderRender: {
      type: 'boolean',

      example: '当前路由不展示菜单顶栏',
    },
    flatMenu: {
      type: 'boolean',
      example: '子项往上提，只是不展示父菜单',
    },
    parentKeys: {
      type: 'string',
      example: '当此节点被选中的时候也会选中 parentKeys 的节点',
    },
    verb: {
      enum: [ 'head', 'options', 'get', 'put', 'post', 'patch', 'del', 'redirect', 'resources' ],
      type: 'string',
      example: '用户触发动作，支持 get，post 等所有 HTTP 方法',
    },
    middleware: {
      type: 'string',
      example: '在 Router 里面可以配置多个 Middleware',
    },
    ignoreMiddleware: {
      type: 'string',
      example: '排除模块统一设置的middleware',
    },
    controller: {
      type: 'string',
      example: 'controller',
    },
    action: {
      type: 'string',
      example: '控制器的方法',
    },
    admin: {
      type: 'boolean',
      example: 'true后台falase前台',
    },
    mod_id: {
      type: 'integer',
      example: '模块Id',
    },
    pid: {
      type: 'integer',
      example: '路由父id',
    },
    sort: {
      type: 'integer',
      example: '越小越靠前',
    },
    children: {
      type: 'array',
      itemType: 'menuItem',
      example: 'itemType[]',
    },
  },
  menuResponse: {
    success: { type: 'boolean', required: true, example: true },
    data: { type: 'array', required: true, example: 'itemType[]', itemType: 'menuItem' },
    errorCode: { type: 'string', required: true, example: '0' },
    errorMessage: { type: 'string', required: true, example: 'ok' },
  },
  modRequest: {
    id: {
      type: 'integer',
      example: 'id',
    },
    name: {
      type: 'string',
      required: true,
      example: '模块名称',
    },
    path: {
      type: 'string',
      required: true,
      example: '模块路径',
    },
    middleware: {
      type: 'string',
      example: '在 Router 里面可以配置多个 Middleware',
    },
    remarks: {
      type: 'string',
      example: '备注',
    },
    sort: {
      type: 'integer',
      example: '越小越靠前',
    },
  },
  modeInfoResponse: {
    success: { type: 'boolean', required: true, example: true },
    data: { type: 'modRequest', required: true, example: 'modRequest' },
    errorCode: { type: 'string', required: true, example: '0' },
    errorMessage: { type: 'string', required: true, example: 'ok' },
  },
  modeListResponse: {
    success: { type: 'boolean', required: true, example: true },
    data: { type: 'array', required: true, example: 'modRequest[]', itemType: 'modRequest' },
    errorCode: { type: 'string', required: true, example: '0' },
    errorMessage: { type: 'string', required: true, example: 'ok' },
  },
};
