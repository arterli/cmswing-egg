'use strict';
module.exports = {
  accountRequest: {
    username: { type: 'string', required: true, description: '用户名', example: 'Tom' },
    password: { type: 'string', required: true, description: '密码', example: '111111' },
    type: { type: 'string', required: true, description: '类型', example: 'account｜mobile' },
  },
  accountResponse: {
    success: { type: 'boolean', required: true, example: true },
    token: { type: 'string', required: true, example: 'token' },
    type: { type: 'string', example: 'account｜mobile' },
    msg: { type: 'string', required: true, example: '登录成功' },
  },
};
