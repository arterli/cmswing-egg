'use strict';
module.exports = {
  orgItem: {
    id: { type: 'integer', required: true, example: '1' },
    name: { type: 'string', required: true, example: 'name' },
    desc: { type: 'string', example: 'desc' },
    pid: { type: 'integer', required: true, example: '1' },
    sort: { type: 'integer', example: '0' },
  },
  addOrgRequest: {
    name: { type: 'string', required: true, example: 'name' },
    desc: { type: 'string', example: 'desc' },
    pid: { type: 'integer', required: true, example: '1' },
    sort: { type: 'integer', example: '0' },
  },
  roleTtem: {
    id: { type: 'integer', required: true, example: '1' },
    name: { type: 'string', required: true, example: 'name' },
    desc: { type: 'string', example: 'desc' },
    menu_ids: { type: 'string', required: true, example: '1' },
    state: { type: 'boolean', example: '0' },
  },
  addRoleRequest: {
    name: { type: 'string', required: true, example: 'name' },
    desc: { type: 'string', example: 'desc' },
    menu_ids: { type: 'string', required: true, example: '1' },
    state: { type: 'boolean', example: '0' },
  },
  userItem: {
    id: { type: 'integer', example: '用户id' },
    username: { type: 'string', example: '用户名' },
    password: { type: 'string', example: '密码' },
    email: { type: 'string', example: '邮箱' },
    mobile: { type: 'string', example: '手机号' },
    state: { type: 'integer', example: '状态0禁用,1正常,-1删除' },
    org_id: { type: 'integer', example: '组织id' },
    admin: { type: 'boolean', example: '系统管理员' },
  },
  userListResponse: {
    success: { type: 'boolean', required: true, example: true },
    total: { type: 'integer', required: true, example: 0 },
    data: { type: 'array', required: true, itemType: 'userItem' },
  },
};
