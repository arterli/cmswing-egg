'use strict';
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;
  const CwMenu = app.model.define('cw_menu', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      comment: 'id',
    },
    name: {
      type: DataTypes.STRING,
      comment: '路由名称',
    },
    path: {
      type: DataTypes.STRING,
      comment: '路由 URL 路径',
    },
    icon: {
      type: DataTypes.STRING,
      comment: '配置菜单的图标，默认使用 antd 的 icon 名，默认不适用二级菜单的 icon',
    },
    access: {
      type: DataTypes.STRING,
      comment: '权限配置，需要预先配置权限',
    },
    hideChildrenInMenu: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      comment: '用于隐藏不需要在菜单中展示的子路由',
    },
    hideInMenu: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      comment: '可以在菜单中不展示这个路由，包括子路由',
    },
    hideInBreadcrumb: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      comment: '可以在面包屑中不展示这个路由，包括子路由。',
    },
    headerRender: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      comment: '当前路由不展示顶栏',
    },
    footerRender: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      comment: '当前路由不展示页脚',
    },
    menuRender: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      comment: '当前路由不展示菜单',
    },
    menuHeaderRender: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      comment: '当前路由不展示菜单顶栏',
    },
    flatMenu: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      comment: '子项往上提，只是不展示父菜单',
    },
    parentKeys: {
      type: DataTypes.STRING,
      comment: '当此节点被选中的时候也会选中 parentKeys 的节点',
    },
    verb: {
      type: DataTypes.ENUM('head', 'options', 'get', 'put', 'post', 'patch', 'del', 'redirect', 'resources'),
      defaultValue: 'get',
      comment: '用户触发动作，支持 get，post 等所有 HTTP 方法',
    },
    middleware: {
      type: DataTypes.STRING,
      comment: '在 Router 里面可以配置多个 Middleware',
    },
    ignoreMiddleware: {
      type: DataTypes.STRING,
      comment: '排除模块统一设置的middleware',
    },
    controller: {
      type: DataTypes.STRING,
      comment: 'controller',
    },
    action: {
      type: DataTypes.STRING,
      comment: '控制器的方法',
    },
    admin: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      comment: 'true后台falase前台',
    },
    role: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      comment: '是否为角色权限节点',
    },
    mod_id: {
      type: DataTypes.INTEGER,
      comment: '模块Id',
    },
    pid: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '路由父id',
    },
    sort: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '越小越靠前',
    },
    createdAt: {
      type: DataTypes.DATE,
      get() {
        return moment(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    updatedAt: {
      type: DataTypes.DATE,
      get() {
        return moment(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
  });
  CwMenu.associate = function() {
    app.model.CwMenu.belongsTo(app.model.CwMenuMod, {
      foreignKey: 'mod_id',
      targetKey: 'id',
      constraints: false,
    });
  };
  CwMenu.sync({ alter: true });
  return CwMenu;
};
