'use strict';
module.exports = app => {
  const DataTypes = app.Sequelize;
  const CwOrg = app.model.define('cw_org',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: 'id',
      },
      name: {
        type: DataTypes.STRING,
        comment: '组织名称',
      },
      desc: {
        type: DataTypes.STRING,
        comment: '组织说明',
      },
      pid: {
        type: DataTypes.INTEGER,
        defaultValue: true,
        comment: '父ID',
      },
      sort: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        comment: '越小越靠前',
      },
    });
  CwOrg.sync({ alter: true });
  return CwOrg;
};
