'use strict';
module.exports = app => {
  const DataTypes = app.Sequelize;
  const CwRole = app.model.define('cw_role', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      comment: 'id',
    },
    name: {
      type: DataTypes.STRING,
      comment: '角色名称',
    },
    desc: {
      type: DataTypes.STRING,
      comment: '角色描述',
    },
    menu_ids: {
      type: DataTypes.TEXT,
      comment: '权限id',
    },
    checkedKeysValue: {
      type: DataTypes.TEXT,
      comment: '选择节点',
    },
    halfCheckedKeys: {
      type: DataTypes.TEXT,
      comment: '选中的父节点',
    },
    state: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      comment: '状态',
    },
  });
  CwRole.sync({ alter: true });
  return CwRole;
};
