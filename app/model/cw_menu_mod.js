'use strict';
const moment = require('moment');
module.exports = app => {
  const DataTypes = app.Sequelize;
  const CwMenuMod = app.model.define('cw_menu_mod', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      comment: 'id',
    },
    name: {
      type: DataTypes.STRING,
      comment: '模块名称',
    },
    path: {
      type: DataTypes.STRING,
      comment: '模块路径',
    },
    middleware: {
      type: DataTypes.STRING,
      comment: '在 Router 里面可以配置多个 Middleware',
    },
    remarks: {
      type: DataTypes.STRING,
      comment: '备注',
    },
    sort: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      comment: '越小越靠前',
    },
    sys: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      comment: '是否系统',
    },
    createdAt: {
      type: DataTypes.DATE,
      get() {
        return moment(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    updatedAt: {
      type: DataTypes.DATE,
      get() {
        return moment(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm:ss');
      },
    },
  });
  CwMenuMod.sync({ alter: true });
  return CwMenuMod;
};
