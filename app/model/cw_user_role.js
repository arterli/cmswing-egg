'use strict';
module.exports = app => {
  const DataTypes = app.Sequelize;
  const CwUserRole = app.model.define('cw_user_role',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        comment: 'id',
      },
      user_id: {
        type: DataTypes.INTEGER,
        comment: '用户id',
      },
      role_id: {
        type: DataTypes.INTEGER,
        comment: '角色ID',
      },
    },
    { indexes: [{ fields: [ 'user_id' ] }, { fields: [ 'role_id' ] }] }
  );
  CwUserRole.associate = function() {
    app.model.CwUser.belongsToMany(app.model.CwRole, {
      through: { model: CwUserRole, unique: false },
      foreignKey: 'user_id',
      otherKey: 'role_id',
      constraints: false,
    });
    app.model.CwRole.belongsToMany(app.model.CwUser, {
      through: { model: CwUserRole, unique: false },
      foreignKey: 'role_id',
      otherKey: 'user_id',
      constraints: false,
    });
  };
  CwUserRole.sync({ alter: true });
  return CwUserRole;
};
