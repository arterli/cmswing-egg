'use strict';
const Service = require('egg').Service;
const path = require('path');
class CwMiddlewareService extends Service {
  async getList() {
    const appDir = path.join(this.app.baseDir, 'app', 'middleware');
    const getFiles = await this.ctx.helper.cw.getFiles(appDir);
    const res = [];
    for (const file of getFiles) {
      const obj = {};
      obj.label = file.name;
      obj.value = file.name;
      res.push(obj);
    }
    return res;
  }
}
module.exports = CwMiddlewareService;
