'use strict';
const Service = require('egg').Service;
const path = require('path');
class CwControllerService extends Service {
  async getList() {
    const appDir = path.join(this.app.baseDir, 'app', 'controller');
    const getFiles = await this.ctx.helper.cw.getFiles(appDir);
    const res = [];
    for (const file of getFiles) {
      const obj = {};
      obj.label = file.name;
      obj.value = file.name;
      obj.path = this.ctx.helper.cw.cipher(file.path);
      res.push(obj);
    }
    return res;
  }
  async getFun(c = '') {
    // console.log(this.app.controller['adminApi']['cwMenu']);
    const cl = c.split('.');
    const clc = cl.length;
    let co = {};
    try {
      if (clc === 1) {
        co = this.app.controller[cl[0]];
      } else if (clc === 2) {
        co = this.app.controller[cl[0]][cl[1]];
      } else if (clc === 3) {
        co = this.app.controller[cl[0]][cl[1]][cl[2]];
      } else if (clc === 4) {
        co = this.app.controller[cl[0]][cl[1]][cl[2]][cl[3]];
      }
    } catch (e) {
      console.log(e);
    }
    const res = [];
    for (const v in co) {
      const obj = {};
      obj.label = v;
      obj.value = v;
      res.push(obj);
    }
    return res;
  }
}
module.exports = CwControllerService;
