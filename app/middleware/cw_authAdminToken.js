'use strict';
module.exports = options => {
  return async function authAdminToken(ctx, next) {
    // 支持 options.exclude
    if (options && ctx.helper.cw._.find(options.exclude, o => ctx.url.indexOf(o) !== -1)) return await next();
    const token = ctx.get('token');
    const userInfo = ctx.helper.cw.deToken(token);
    if (userInfo) {
      ctx.userInfo = userInfo;
      await next();
    } else {
      ctx.body = {
        data: {
          isLogin: false,
        },
        errorCode: '401',
        errorMessage: '请先登录！',
        success: true,
      };
    }

  };
};
