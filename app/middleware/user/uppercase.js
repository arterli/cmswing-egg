module.exports = () => {
  return async function uppercase(ctx, next) {
    ctx.logger.info('uppercase data: %j', ctx.params);
    ctx.params.id = ctx.params.id && ctx.params.id.toUpperCase();
    ctx.query.name = ctx.query.name && ctx.query.name.toUpperCase();
    await next();
  };
};
